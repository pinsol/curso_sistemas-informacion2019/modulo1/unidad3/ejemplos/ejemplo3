﻿                                              /** Modulo I - Unidad 3 - Hoja Ejemplos 3 **/
DROP DATABASE IF EXISTS ejemploprogramacion3;
CREATE DATABASE IF NOT EXISTS ejemploprogramacion3;
USE ejemploprogramacion3;

SELECT * FROM alumnos;
SELECT * FROM circulo;
SELECT * FROM conversion;
SELECT * FROM cuadrados;
SELECT * FROM grupos;
SELECT * FROM rectangulo;
SELECT * FROM triangulos;

/*
  Ejemplo 1-
  Hemos insertado en la base de datos las tablas
  con sus registros desde un archivo de excel
*/

/* 
  Ejemplo 2-
  Función área triangulo
  
  Argumentos:
   - Base de un triangulo
   - Altura de un triangulo
  
  Debe devolver
   - Área del triángulo
*/
DELIMITER //
CREATE OR REPLACE FUNCTION triarea(base float, altura float)
  RETURNS float
COMMENT
  "Funcion que devuelve el area del triangulo introduciendo los
   argumentos base y altura del triangulo a calcular."
BEGIN
  RETURN base*altura/2;
END //
DELIMITER ;

SELECT triarea(2,3);

/* 
  Ejemplo 3-
  Función perimetro triangulo
  
  Argumentos:
   - Base de un triangulo
   - Lado2 de un triangulo
   - Lado3 de un triangulo  
  Debe devolver
   - Perímetro del triángulo
*/
DELIMITER //
CREATE OR REPLACE FUNCTION triper(base float, lado1 float, lado2 float)
  RETURNS float
COMMENT
  "Funcion que devuelve el perimetro del triangulo introduciendo los
   argumentos base lado1 y lado2 del triangulo a calcular."
BEGIN
  RETURN base+lado1+lado2;
END //
DELIMITER ;

SELECT triper(5,2,3);

/*
  Ejemplo 4-
  Procedimiento almacenado que actualice el área y el perímetro de los triángulos
  (utilizando las funciones realizadas) que estén comprendidos entre los argumentos pedidos:
  
  Argumentos:
   - Id1: id inicial
   - Id2: id final
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE tricomplet(id1 int, id2 int)
COMMENT
  "Procedimiento que rellena los campos correspondientes al
  area y el perimetro de los registros que hay desde la primera
  id hasta la segunda id introducidas."
  BEGIN
     UPDATE triangulos t
      SET t.area=triarea(t.base,t.altura),
          t.perimetro=triper(t.base,t.lado2,t.lado3)
      WHERE id BETWEEN IF(id1<id2,id1,id2) AND IF(id2>id1,id2,id1)
      ;
    
  END //
DELIMITER ;

CALL tricomplet(2, 5);
CALL tricomplet(12, 9);

SELECT * FROM triangulos t;

/* 
  Ejemplo 5-
  Función área cuadrado
  
  Argumentos:
   - Lado del cuadrado
  
  Debe devolver
   - Área del cuadrado
*/
DELIMITER //
CREATE OR REPLACE FUNCTION sqarea(lado float)
  RETURNS float
COMMENT
  "Funcion que devuelve el area del cuadrado introduciendo el
   argumento lado del cuadrado a calcular."
BEGIN
  RETURN POW(lado,2);
END //
DELIMITER ;

SELECT sqarea(2);

/* 
  Ejemplo 6-
  Función perimetro cuadrado
  
  Argumentos:
   - Lado del cuadrado  
  Debe devolver
   - Perímetro del cuadrado
*/
DELIMITER //
CREATE OR REPLACE FUNCTION sqper(lado float)
  RETURNS float
COMMENT
  "Funcion que devuelve el perimetro del cuadrado introduciendo de
   argumento el lado del cuadrdo a calcular."
BEGIN
  RETURN lado*4;
END //
DELIMITER ;

SELECT sqper(5);

/*
  Ejemplo 7-
  Procedimiento almacenado que actualice el área y el perímetro de los cuadrados
  (utilizando las funciones realizadas) que estén comprendidos entre los argumentos pedidos:
  
  Argumentos:
   - Id1: id inicial
   - Id2: id final
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE sqcomplet(id1 int, id2 int)
COMMENT
  "Procedimiento que rellena los campos de la tabla cuadrados
  correspondientes al area y el perimetro de los registros que
  hay desde la primera id hasta la segunda id introducidas."
  BEGIN
     UPDATE cuadrados c
      SET c.area=sqarea(c.lado),
          c.perimetro=sqper(c.lado)
      WHERE id BETWEEN IF(id1<id2,id1,id2) AND IF(id2>id1,id2,id1)
      ;
    
  END //
DELIMITER ;

CALL sqcomplet(2, 9);

SELECT * FROM cuadrados c;


/* 
  Ejemplo 8-
  Función área rectángulo
  
  Argumentos:
   - Lado1 del rectangulo
  
  Debe devolver
   - Área del rectangulo
*/
DELIMITER //
CREATE OR REPLACE FUNCTION rectarea(lado1 float, lado2 float)
  RETURNS float
COMMENT
  "Funcion que devuelve el area del rectangulo introduciendo
   los argumentos lado1 y lado2 del cuadrado a calcular."
BEGIN
  RETURN lado1*lado2;
END //
DELIMITER ;

SELECT rectarea(3,5);

/* 
  Ejemplo 9-
  Función perimetro cuadrado
  
  Argumentos:
   - Lado del cuadrado  
  Debe devolver
   - Perímetro del cuadrado
*/
DELIMITER //
CREATE OR REPLACE FUNCTION rectper(lado1 float, lado2 float)
  RETURNS float
COMMENT
  "Funcion que devuelve el perimetro del rectangulo introduciendo los
   argumentos lado1 y lado2 del cuadrado a calcular."
BEGIN
  RETURN 2*lado1+2*lado2;
END //
DELIMITER ;

SELECT rectper(3,5);

/*
  Ejemplo 10-
  Procedimiento almacenado que actualice el área y el perímetro de los rectangulos
  (utilizando las funciones realizadas) que estén comprendidos entre los argumentos pedidos:
  
  Argumentos:
   - Id1: id inicial
   - Id2: id final
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE rectcomplet(id1 int, id2 int)
COMMENT
  "Procedimiento que rellena los campos de la tabla rectangulos
  correspondientes al area y el perimetro de los registros que
  hay desde la primera id hasta la segunda id introducidas."
  BEGIN
     UPDATE rectangulo r
      SET r.area=rectarea(r.lado1, r.lado2),
          r.perimetro=rectper(r.lado1, r.lado2)
      WHERE id BETWEEN IF(id1<id2,id1,id2) AND IF(id2>id1,id2,id1)
      ;
    
  END //
DELIMITER ;

CALL rectcomplet(7, 13);

SELECT * FROM rectangulo r;

/* 
  Ejemplo 11-
  Función área circulo
  
  Argumentos:
   - Radio del circulo
  
  Debe devolver
   - Área del circulo
*/
DELIMITER //
CREATE OR REPLACE FUNCTION circarea(radio float)
  RETURNS float
COMMENT
  "Funcion que devuelve el area del circulo introduciendo
   el argumento radio del circulo a calcular."
BEGIN
  RETURN PI()*POW(radio,2);
END //
DELIMITER ;

SELECT circarea(5);

/* 
  Ejemplo 12-
  Función perimetro circulo
  
  Argumentos:
   - Radio del circulo  
  Debe devolver
   - Perímetro del circulo
*/
DELIMITER //
CREATE OR REPLACE FUNCTION circper(radio float)
  RETURNS float
COMMENT
  "Funcion que devuelve el perimetro del circulo introduciendo el
   argumento radio del circulo a calcular."
BEGIN
  RETURN 2*PI()*POW(radio,2);
END //
DELIMITER ;

SELECT circper(5);

/*
  Ejemplo 13-
  Procedimiento almacenado que actualice el área y el perímetro de los circulos
  (utilizando las funciones realizadas) que estén comprendidos entre los argumentos pedidos:
  
  Argumentos:
   - Id1: id inicial
   - Id2: id final
   - tipo: a, b o null
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE circcomplet(id1 int, id2 int, tipo char(4))
COMMENT
  "Procedimiento que rellena los campos de la tabla circulo
  correspondientes al area y el perimetro de los registros que
  hay desde la primera id hasta la segunda id y el tipo introducidos."
  BEGIN
     IF (tipo IS NULL) THEN
        UPDATE circulo SET
          area = circarea(radio),
          perimetro = circper(radio)
        WHERE id BETWEEN id1 AND id2;
      ELSE
        UPDATE circulo c SET
          area = circarea(radio),
          perimetro = circper(radio)
        WHERE id BETWEEN id1 AND id2 
        AND c.tipo = tipo;
      END IF;

    
  END //
DELIMITER ;

CALL circcomplet(7, 13, null);

SELECT * FROM circulo c;

/*
  Ejemplo 14-
  Función media

  Argumentos:
   - nota1, nota2, nota3 y nota4

  Devuelve:
  Resultado de la media de las cuatro notas
*/
DELIMITER //
CREATE OR REPLACE FUNCTION media(nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN

  RETURN (nota1+nota2+nota3+nota4)/4;
END //
DELIMITER ;

SELECT media(7,7,8,5);

/*
  Ejemplo 15-
  Función minimo

  Argumentos:
   - nota1, nota2, nota3 y nota4

  Devuelve:
  Resultado de la nota minima de las cuatro notas
*/
DELIMITER //
CREATE OR REPLACE FUNCTION minimo(nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN

  RETURN LEAST(nota1,nota2,nota3,nota4);
END //
DELIMITER ;

SELECT minimo(7,7,8,5);

/*
  Ejemplo 16-
  Función maximo

  Argumentos:
   - nota1, nota2, nota3 y nota4

  Devuelve:
  Resultado de la nota maxima de las cuatro notas
*/
DELIMITER //
CREATE OR REPLACE FUNCTION maximo(nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN

  RETURN GREATEST(nota1,nota2,nota3,nota4);
END //
DELIMITER ;

SELECT maximo(7,7,8,5);

/*
  Ejemplo 17-
  Función moda

  Argumentos:
   - nota1, nota2, nota3 y nota4

  Devuelve:
  Resultado de la nota mas repetida de las cuatro notas
*/
DELIMITER //
CREATE OR REPLACE FUNCTION moda(nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN

  DECLARE moda float DEFAULT 0;
  DECLARE maxi int DEFAULT 0;
      
  CREATE OR REPLACE TEMPORARY TABLE notas(
     id int AUTO_INCREMENT PRIMARY KEY,
     nota float
    );

     INSERT INTO notas (nota) VALUES 
     (nota1),(nota2),(nota3),(nota4);
    
   SELECT MAX(repe) INTO maxi 
     FROM(
       SELECT
        nota,
        COUNT(*)repe 
       FROM
        notas 
         GROUP BY nota
       )c1;
      
    
   SELECT AVG(nota) INTO moda 
     FROM (
       SELECT nota,
        COUNT(*)repe
       FROM
        notas 
         GROUP BY nota
        )c1
     WHERE c1.repe=maxi;
            
   RETURN moda;
END //
DELIMITER ;

SELECT moda(7,7,8,5);

SELECT * FROM alumnos a;


/*
  Ejemplo 18-
  Procedimiento almacenado que actualice media, maxima, minima, moda de los registros de alumnos
  (utilizando las funciones realizadas) que estén comprendidos entre los argumentos pedidos y el grupo:
  
  Argumentos:
   - Id1: id inicial
   - Id2: id final
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE alumcomplet(id1 int, id2 int)
  BEGIN
      
      UPDATE alumnos
        set min = minimo(nota1, nota2, nota3, nota4), 
          max = maximo(nota1, nota2, nota3, nota4),
          media = media(nota1, nota2, nota3, nota4),
          moda = moda(nota1, nota2, nota3, nota4)
      WHERE id BETWEEN id1 AND id2; 
      
      UPDATE grupos
        set media = (SELECT AVG(a.media) medg1 FROM alumnos a),
          max = (SELECT MAX(a.max) maxg1 FROM alumnos a),
          min = (SELECT MIN(a.min) ming1 FROM alumnos a)
        WHERE id = 1;

      UPDATE grupos
        set media = (SELECT AVG(media) medg2 FROM alumnos a),
          max = (SELECT MAX(a.max) maxg2 FROM alumnos a),
          min = (SELECT MIN(a.min) ming2 FROM alumnos a) 
        WHERE id = 2;
        
    END //
  DELIMITER ;

  CALL alumcomplet(1,7);
  SELECT * FROM alumnos a;
  SELECT * FROM grupos g;


/*
  Ejemplo 19-
  Procedimiento almacenado que le pasas como argumento un id y te calcula la conversión de unidades de
todos los registros que estén detrás de ese id.

  Argumentos:
   - Id: id inicial
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE convers(id1 int)
BEGIN
     UPDATE conversion c
        SET m = cm/100 ,
          km = cm/100000,
          pulgadas = cm/2.54
      WHERE c.id > id1 AND c.cm IS NOT NULL;
     
      UPDATE conversion c
        SET cm = m*100,
          km = m/1000,
          pulgadas = m*39.3701
      WHERE c.id > id1 AND c.m IS NOT NULL;
          
      UPDATE conversion c
        SET cm = km*100000,
          m = km*1000,
          pulgadas = km*39370.1
      WHERE c.id > id1 AND c.km IS NOT NULL;
     
      UPDATE conversion c
        SET cm = pulgadas*2.54,
          m = pulgadas/0.0254,
          km = pulgadas/39370.1
      WHERE c.id > id1 AND c.pulgadas IS NOT NULL;

END //
DELIMITER ;

CALL convers(7);

SELECT * FROM circulo c;

SELECT * FROM conversion c;